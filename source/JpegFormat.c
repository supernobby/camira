/*****************************************************************************

    This file is part of cAMIra - a small webcam application for the AMIGA.
    
    Copyright (C) 2012-2020 Andreas (supernobby) Barth

    cAMIra is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cAMIra is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cAMIra. If not, see <http://www.gnu.org/licenses/>.
    
*****************************************************************************/


/*
** JpegFormat.c
*/


#include "JpegFormat.h"
#include "Log.h"
#include "CompilerExtensions.h"
#include <proto/exec.h>
#include <proto/jpeg.h>
#include <libraries/jpeg.h>


/*
** progress hook for jpeg library functions
*/
static REGFUNC ULONG JpegProgressHook( REG( a0, struct ImageProcessor *MyImageProcessor ), REG( d0, ULONG Current ), REG( d1, ULONG Total ) )
{
  struct Context *MyContext ;
  
  MyContext = MyImageProcessor->ip_Context ;

  //Dispatcher( MyContext->c_Dispatcher, NONBLOCKING_MODE ) ;  /* dispatch non-blocking */
  
  return( MyContext->c_ShutdownRequest ) ;
}


/*
** create jpeg image based on rgb image data
*/
struct vhi_image *CreateJpegFromRgb24( struct ImageProcessor *MyImageProcessor, struct vhi_image *Rgb24Image )
{
  struct Context *MyContext ;
  struct Library *JpegBase ;
  struct JPEGComHandle *JpegEncoder ;
  ULONG Error ;
  struct vhi_image *JpegImage ;

  JpegImage = NULL ;

  if( ( NULL != MyImageProcessor ) && ( NULL != MyImageProcessor->ip_JpegBase ) && ( NULL != Rgb24Image ) )
  {  /* requirements ok */
    MyContext = MyImageProcessor->ip_Context ;
    JpegBase = MyImageProcessor->ip_JpegBase ;

    JpegImage = AllocImage( MyContext, 0, 0, VHI_JPEG ) ;
    if( NULL != JpegImage )
    {  /* jpeg image struct ok */
      Error = AllocJPEGCompress( &JpegEncoder,
                                 JPG_DestMemStream, ( ULONG )&JpegImage->chunky,
                                 JPG_DestMemStreamSize, ( ULONG )&JpegImage->width,
                                 TAG_END ) ;

      if( !( Error ) )
      {  /* jpeg encoder ok */
        Error = CompressJPEG( JpegEncoder,
                              JPG_SrcRGBBuffer, ( ULONG )Rgb24Image->chunky ,
                              JPG_Width, Rgb24Image->width,
                              JPG_Height, Rgb24Image->height,
                              JPG_Quality, MyImageProcessor->ip_CurrentJpegQuality,
                              JPG_Progressive, MyImageProcessor->ip_CurrentJpegProgressive,
                              JPG_ProgressHook, ( ULONG )JpegProgressHook,
                              JPG_ProgressUserData, ( ULONG )MyImageProcessor,
                              TAG_END );
        if( !( Error ) )
        {  /* jpeg compress ok */
        }
        else
        {  /* jpeg compress not ok */
          LogText( MyContext->c_Log, WARNING_LEVEL, "jpeg compress not ok: %ld", Error ) ;
          FreeImage( MyContext, JpegImage ) ;
          JpegImage = NULL ;
        }
        FreeJPEGCompress( JpegEncoder ) ;
      }
      else
      {  /* jpeg encoder not ok */
        LogText( MyContext->c_Log, WARNING_LEVEL, "jpeg encoder not ok: %ld", Error ) ;
        FreeImage( MyContext, JpegImage ) ;
        JpegImage = NULL ;
      }
    }
    else
    {  /* jpeg image struct not ok */
      LogText( MyContext->c_Log, WARNING_LEVEL, "jpeg image struct not ok" ) ;
    }
  }
  else
  {  /* requirements not ok */
  }

  return( JpegImage ) ;
}


/*
** create rgb image based on jpeg image
*/
struct vhi_image *CreateRgb24FromJpeg( struct ImageProcessor *MyImageProcessor, struct vhi_image *JpegImage )
{
  struct Context *MyContext ;
  struct Library *JpegBase ;
  struct JPEGDecHandle *JpegDecoder ;
  ULONG Error ;
  ULONG Width, Height, BytePerPixel ;
  UBYTE ColourSpace ;
  struct vhi_image *Rgb24Image ;
  
  Rgb24Image = NULL ;
  
  if( ( NULL != MyImageProcessor ) && ( NULL != MyImageProcessor->ip_JpegBase ) && ( NULL != JpegImage ) )
  {  /* parameter seem to be ok */
    MyContext = MyImageProcessor->ip_Context ;
    JpegBase = MyImageProcessor->ip_JpegBase ;
    
    Error = AllocJPEGDecompress( &JpegDecoder,
                                 JPG_SrcMemStream, ( ULONG )JpegImage->chunky,
                                 JPG_SrcMemStreamSize, JpegImage->width,
                                 TAG_END ) ;
    if( !( Error ) )
    {  /* jpeg decoder ok */
      Error = GetJPEGInfo( JpegDecoder,
                           JPG_Width, ( ULONG )&Width,
                           JPG_Height, ( ULONG )&Height,
                           JPG_ColourSpace, ( ULONG )&ColourSpace,
                           JPG_BytesPerPixel, ( ULONG )&BytePerPixel,
                           TAG_END ) ;
      if( !( Error ) )
      {  /* got jpeg infos */
        if( ( 3 == BytePerPixel ) && ( JPCS_RGB == ColourSpace ) )
        {  /* rgb 24 bit per pixel format */
          Rgb24Image = AllocImage( MyContext, Width, Height, VHI_RGB_24 ) ;
        }
        if( NULL != Rgb24Image )
        {  /* rgb image struct ok */
          Error = DecompressJPEG( JpegDecoder, 
                                  JPG_DestRGBBuffer, ( ULONG )Rgb24Image->chunky,
                                  JPG_ProgressHook, ( ULONG )JpegProgressHook,
                                  JPG_ProgressUserData, ( ULONG )MyImageProcessor,
                                  TAG_END ) ;
          if( !( Error ) )
          {  /* jpeg decompression ok */
          }
          else
          {  /* jpeg decompression not ok */
            LogText( MyContext->c_Log, WARNING_LEVEL, "jpeg decompression not ok: %ld", Error ) ;
            FreeImage( MyContext, Rgb24Image ) ;
            Rgb24Image = NULL ;
          }
        }
        else
        {  /* rgb image struct not ok */
          LogText( MyContext->c_Log, WARNING_LEVEL, "rgb image struct not ok" ) ;
        }
      }
      else
      {  /* could not get jpeg infos */
        LogText( MyContext->c_Log, WARNING_LEVEL, "could not get jpeg infos: %ld", Error ) ;
      }
      FreeJPEGDecompress( JpegDecoder ) ;
    }
    else
    {  /* jpeg decoder not ok */
      LogText( MyContext->c_Log, WARNING_LEVEL, "jpeg decoder not ok: %ld", Error ) ;
    }
  }
  else
  {  /* parameter not ok */
  }
  
  return( Rgb24Image ) ;
}
